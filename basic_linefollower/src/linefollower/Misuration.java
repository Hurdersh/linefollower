package linefollower;

import lejos.hardware.BrickFinder;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.TextLCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.SensorMode;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;
import lejos.hardware.sensor.EV3UltrasonicSensor;

public class Misuration {
		private float black[]; /*valori acquisiti all'inizio*/
		private float white[];
		private float right[]; /*valori da aggiornare costantemente*/
		private float left[];
		private float center[];
		private float distance[];
		private final EV3 ev3 = (EV3) BrickFinder.getLocal();
		private SensorMode left_rgb;
		private SensorMode right_rgb;
		private SensorMode center_rgb;
		private EV3UltrasonicSensor ultrasoundSensor;
		private SampleProvider sampleProvider;
		
		public Misuration() {
			//misuration of left values
			EV3ColorSensor cs1	= new EV3ColorSensor(SensorPort.S1);	
			this.left_rgb = cs1.getColorIDMode();
			this.left = new float[left_rgb.sampleSize()];
			this.white = new float[left_rgb.sampleSize()];
			//misuration of right values
			EV3ColorSensor cs2	= new EV3ColorSensor(SensorPort.S3);	
			this.right_rgb = cs2.getColorIDMode();
			this.right = new float[right_rgb.sampleSize()];
			
			//misuration of center values
			EV3ColorSensor cs3	= new EV3ColorSensor(SensorPort.S2);	
			this.center_rgb = cs3.getColorIDMode();
			this.center = new float[center_rgb.sampleSize()];
			this.black = new float[center_rgb.sampleSize()];
			
			//misuration of ultrasound sensor
			this.ultrasoundSensor = new EV3UltrasonicSensor(SensorPort.S4);
			sampleProvider = ultrasoundSensor.getDistanceMode();
			this.distance = new float[sampleProvider.sampleSize()];
			
			
			this.initialValues();
			this.updateValues();
		}
		
		private boolean isBlack(float v[]) {
			if(v[0] == black[0]) {
				return true;
			}
			else {
			return false;
			}
		}
		private boolean isWhite(float v[]) {
			if(v[0] == white[0]) {
				return true;
			}
			else {
				return false;
			}
		}
		private boolean isGreen(float v[]) {
			if(!this.isBlack(v) && !this.isWhite(v)) {
				return true;
			}
			else
				return false;
		}
		public boolean whatIs(String sensor, String color) {
			if(sensor == "right") {
				if(color == "black") {
					return this.isBlack(this.right);
				}
				else if(color == "white") {
					return this.isWhite(this.right);
				}
				else if(color == "green") {
					return this.isGreen(this.right);
				}
			}
			else if (sensor == "left") {
				if(color == "black") {
					return this.isBlack(this.left);
				}
				else if(color == "white") {
					return this.isWhite(this.left);
				}
				else if(color == "green") {
					return this.isGreen(this.left);
				}
			}
			else if (sensor == "center") {
				if(color == "black") {
					return this.isBlack(this.center);
				}
				else if(color == "white") {
					return this.isWhite(this.center);
				}
				else if(color == "green") {
					return this.isGreen(this.center);
				}
		}
			return false;
		}
		//method used to misurate distance 
		public boolean getDistance(double distanza)
		{
					
			if(distance[0] <= distanza)
				return true;
			else
				return false;
			
		}
		
		private void initialValues() {
			//initial misuration of black values
			this.left_rgb.fetchSample(this.white, 0); //il sensore sinistro misura il bianco
			//initial misuration of white values
			this.center_rgb.fetchSample(this.black, 0); //il sensore al centro misura il nero
		
		}
		
		public void updateValues() {
			//fetch left values
			left_rgb.fetchSample(this.left, 0);
			//misuration of right values
			right_rgb.fetchSample(this.right, 0);
			//misuration of center values
			center_rgb.fetchSample(this.center, 0);
			sampleProvider.fetchSample(this.distance, 0);
		}
		
}
