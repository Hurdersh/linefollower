package linefollower;
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.TextLCD;
import lejos.hardware.motor.BaseRegulatedMotor;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.SensorMode;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;
public class MainProgram {

	public static void main(String[] args) {
		Misuration m = new Misuration();
		//declaration of lcd
		final EV3 ev3 = (EV3) BrickFinder.getLocal();
		TextLCD lcd = ev3.getTextLCD();
		// declare and use motors a and d
		RegulatedMotor left = new EV3LargeRegulatedMotor(MotorPort.A);
		RegulatedMotor right = new EV3LargeRegulatedMotor(MotorPort.D);
		 while(Button.ESCAPE.isUp()) {
			m.updateValues();
			left.setSpeed(200);
			right.setSpeed(200);
			
			if(m.getDistance(0.15)) {
				lcd.drawString("vedo sperlung", 0, 0);
				left.forward();
				right.backward();
				Delay.msDelay(2000);
				left.forward();
				right.forward();
				Delay.msDelay(3500);
				left.backward();
				right.forward();
				Delay.msDelay(2000);
				left.forward();
				right.forward();
				Delay.msDelay(5000);
				left.backward();
				right.forward();
				Delay.msDelay(1000);
				while(!m.whatIs("right", "black")) {
					right.forward();
					left.forward();
					m.updateValues();
				}
				m.updateValues();
			}
			else if(m.whatIs("right", "white") && m.whatIs("center", "black") && m.whatIs("left", "white")) {
				left.forward();
				right.forward();
			}
			else if(m.whatIs("right", "black") && m.whatIs("center", "white") && m.whatIs("left", "white")){
				while(!m.whatIs("right", "white") || !m.whatIs("center", "black") || !m.whatIs("right", "white")) {
					right.backward();
					left.forward();
					m.updateValues();
				}
			}
			else if(m.whatIs("right", "white") && m.whatIs("center", "white") && m.whatIs("left", "black")) {
				while(!m.whatIs("right", "white") || !m.whatIs("center", "black") || !m.whatIs("right", "white")) {
					right.forward();
					left.backward();
					m.updateValues();
				}
			}
			//this part of code is used to follow line at right and left
			else if(m.whatIs("right", "black") && m.whatIs("center", "black") && m.whatIs("left", "white")){
				while(!m.whatIs("right", "white") && !m.whatIs("center", "white")) {
					right.forward();
					left.forward();
					m.updateValues();
				}
				while(!m.whatIs("right", "white") || !m.whatIs("center", "black") || !m.whatIs("right", "white")) {
					right.backward();
					left.forward();
					m.updateValues();
				}
			}
			else if(m.whatIs("right", "white") && m.whatIs("center", "black") && m.whatIs("left", "black")) {
			    while(!m.whatIs("left", "white") && !m.whatIs("center", "white")) {
					right.forward();
					left.forward();
					m.updateValues();
				}
				while(!m.whatIs("right", "white") || !m.whatIs("center", "black") || !m.whatIs("left", "white")) {
					right.forward();
					left.backward();
					m.updateValues();
				}
			}
			//2 go straight cases
			else if(m.whatIs("right", "black") && m.whatIs("center", "black") && m.whatIs("left", "black")) {
				left.forward();
				right.forward();
			}
			else if(m.whatIs("right", "white") && m.whatIs("center", "white") && m.whatIs("left", "white")) {
				left.forward();
				right.forward();
			}
			//this portion of code is used to turn according to the green(not white and not black)
			else if(m.whatIs("right", "green") && m.whatIs("center", "black") && m.whatIs("left", "white")) {
				while(!m.whatIs("right", "white")) {
					right.forward();
					left.forward();
					m.updateValues();
				}
				right.backward();
				left.forward();
				Delay.msDelay(500);
				while(!m.whatIs("right", "white") || !m.whatIs("center", "black") || !m.whatIs("left", "white")) {
					right.backward();
					left.forward();
					m.updateValues();
				}
			}
			else if(m.whatIs("right", "white") && m.whatIs("center", "black") && m.whatIs("left", "green")) {
				lcd.drawString("GREEN LEFT", 0, 0);
			    while(!m.whatIs("left", "white")) {
					right.forward();
					left.forward();
					m.updateValues();
				}
			    right.forward();
				left.backward();
			    Delay.msDelay(500);
			    lcd.drawString("left bianco e right bianco", 0, 0);
				while(!m.whatIs("right", "white") || !m.whatIs("center", "black") || !m.whatIs("left", "white")) {
					right.forward();
					left.backward();
					m.updateValues();
				}
			}
			else if(m.whatIs("right", "green") && m.whatIs("center", "black") && m.whatIs("left", "green")) {
				left.backward();
				right.backward();
				
				left.backward();
				right.forward();
				while(!m.whatIs("right", "white") || !m.whatIs("center", "black") || !m.whatIs("left", "white")) {
					right.forward();
					left.backward();
					m.updateValues();
				}
			}
				
			
			Delay.msDelay(50);
		 }
	}

}
